package com.ibh.systems.repoman.channel.jpa.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.Event;

import com.ibh.systems.repoman.channel.Artifact;
import com.ibh.systems.repoman.channel.Channel;
import com.ibh.systems.repoman.channel.ChannelManager;

public class ChannelManagerImpl implements ChannelManager {

	private class ChannelImpl implements Channel {
		private String id;

		public ChannelImpl(com.ibh.systems.repoman.channel.jpa.Channel input) {
			this.id = input.getId();
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public Collection<Artifact> getArtifacts() {
			return Collections.emptyList();
		}

		@Override
		public Artifact storeArtifact(InputStream stream) {
			return null;
		}

		@Override
		public void delete() {
			ChannelManagerImpl.this.deleteChannel(id);
		}
	}

	private EventAdmin eventAdmin;

	public void setEventAdmin(EventAdmin eventAdmin) {
		this.eventAdmin = eventAdmin;
	}

	private EntityManagerFactory emf;

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void deleteChannel(String id) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			try {
				com.ibh.systems.repoman.channel.jpa.Channel ref = em
						.getReference(
								com.ibh.systems.repoman.channel.jpa.Channel.class,
								id);
				em.remove(ref);
				em.flush();
				em.getTransaction().commit();
				postEntityChange(id, "delete");
			} catch (Exception e) {
				em.getTransaction().rollback();
				throw new RuntimeException (e);
			}

		} finally {
			em.close();
		}
	}

	private void postEntityChange(String id, String operation) {
		if ( eventAdmin == null )
			return;
		
		HashMap<String, String> properties = new HashMap<String, String>();
		properties.put("id", id);
		properties.put("operation", operation);
	
		eventAdmin.postEvent(new Event("entity/channel", properties));
	}

	@Override
	public Collection<Channel> getChannels() {
		EntityManager em = emf.createEntityManager();
		try {
			CriteriaBuilder cb = emf.getCriteriaBuilder();
			CriteriaQuery<com.ibh.systems.repoman.channel.jpa.Channel> cq = cb
					.createQuery(com.ibh.systems.repoman.channel.jpa.Channel.class);

			TypedQuery<com.ibh.systems.repoman.channel.jpa.Channel> query = em
					.createQuery(cq);
			return convert(query.getResultList());
		} finally {
			em.close();
		}
	}

	private Collection<Channel> convert(
			List<com.ibh.systems.repoman.channel.jpa.Channel> resultList) {
		Collection<Channel> result = new ArrayList<Channel>(resultList.size());

		for (com.ibh.systems.repoman.channel.jpa.Channel input : resultList) {
			Channel output = new ChannelImpl(input);
			result.add(output);
		}

		return result;
	}

	@Override
	public Channel getChannel(String id) {
		EntityManager em = emf.createEntityManager();
		try {
			com.ibh.systems.repoman.channel.jpa.Channel channel = em.find(
					com.ibh.systems.repoman.channel.jpa.Channel.class, id);
			if (channel != null)
				return new ChannelImpl(channel);
			else
				return null;

		} finally {
			em.close();
		}
	}

	@Override
	public Channel createChannel(String id) {
		EntityManager em = emf.createEntityManager();
		try {
			com.ibh.systems.repoman.channel.jpa.Channel channel;
			em.getTransaction().begin();
			try {
				channel = new com.ibh.systems.repoman.channel.jpa.Channel();
				channel.setId(id);
				em.persist(channel);
				em.flush();
				em.getTransaction().commit();
			} catch (Exception e) {
				em.getTransaction().rollback();
				throw new RuntimeException(e);
			}
			postEntityChange(id, "create");
			return new ChannelImpl(channel);
		} finally {
			em.close();
		}
	}

}
