package com.ibh.systems.repoman.channel.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Channel {
	@Id
	private String id;
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
}
