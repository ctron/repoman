package com.ibh.systems.repoman.storage.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import com.google.common.io.ByteStreams;
import com.ibh.systems.repoman.storage.Storage;

public class FileStorage implements Storage {

	private static final String BASE_DIR=System.getProperty("com.ibh.systems.repoman.storage.file.baseDir", System.getProperty("user.home") + "/.repoman/storage");
	private File base;
	
	public FileStorage() {
		this.base = new File ( BASE_DIR );
		if ( !base.exists())
		{
			base.mkdirs();
		}
		if ( !base.isDirectory() )
			throw new RuntimeException ( String.format ("Failed to initialize storage. Directory %s does not exists or is not a directory.", BASE_DIR) );
	}
	
	public void dispose ()
	{
		
	}
	
	@Override
	public UUID storeBlob(InputStream stream) throws IOException {
		UUID uuid = UUID.randomUUID();
		File file = new File ( base, uuid.toString() );
		
		FileOutputStream output = new FileOutputStream(file);
		try{
			ByteStreams.copy(stream, output );
		}
		finally {
			output.close();
			// delete file, since blob was not created
			file.delete();
		}
		
		return uuid;
	}

	@Override
	public void deleteBlob(UUID uuid) throws IOException {
		File file = new File ( base, uuid.toString() );
		if ( !file.exists() )
			return;
		if ( !file.delete() ) {
			throw new IOException ( String.format ("Unable to delete file: %s", file ) );
		}
	}

}
