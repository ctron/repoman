package com.ibh.systems.repoman.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public interface Storage {
	public UUID storeBlob ( InputStream stream ) throws IOException;
	public void deleteBlob ( UUID uuid ) throws IOException;
}
