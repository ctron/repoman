package com.ibh.systems.repoman.channel.console;

import com.ibh.systems.repoman.channel.Channel;
import com.ibh.systems.repoman.channel.ChannelManager;

public class ConsoleImpl {
	
	private ChannelManager channelManager;
	
	public void setChannelManager(ChannelManager channelManager) {
		this.channelManager = channelManager;
	}
	
	public void list ()
	{
		System.out.println("Channels:");
		for ( Channel channel : channelManager.getChannels() )
		{
			System.out.println("  " + channel.getId());
		}
	}
	
	public void add ( String channelId )
	{
		channelManager.createChannel(channelId);
	}
	
	public void delete ( String channelId )
	{
		Channel channel = channelManager.getChannel(channelId);
		if ( channel != null )
		{
			System.out.println("Deleted channel: " + channelId);
			channel.delete();
		}
		else
		{
			System.err.println("Channel not found:" + channelId);
		}
	}
}
