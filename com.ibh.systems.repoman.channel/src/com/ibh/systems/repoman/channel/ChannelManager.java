package com.ibh.systems.repoman.channel;

import java.util.Collection;

public interface ChannelManager {
	public Collection<Channel> getChannels ();
	public Channel getChannel ( String id );
	public Channel createChannel (String id);
}
