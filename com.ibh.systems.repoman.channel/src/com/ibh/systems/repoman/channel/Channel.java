package com.ibh.systems.repoman.channel;

import java.io.InputStream;
import java.util.Collection;

public interface Channel {
	public String getId ();
	public Collection<Artifact> getArtifacts();
	public Artifact storeArtifact ( InputStream stream );
	public void delete ();
}
