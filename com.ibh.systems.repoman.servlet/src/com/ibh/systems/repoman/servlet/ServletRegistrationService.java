package com.ibh.systems.repoman.servlet;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.ServletException;

import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class ServletRegistrationService {

	private HttpService httpService;
	
	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}
	
	public void start () throws ServletException, NamespaceException
	{
		Dictionary<Object,Object> initparams = new Hashtable<Object,Object>();
		httpService.registerServlet("/repo", new MainServlet(), initparams, null);
	}
	
	public void stop ()
	{
		httpService.unregister("/repo");
	}
	
}
