package com.ibh.systems.repoman.app;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.rap.rwt.application.Application;
import org.eclipse.rap.rwt.application.ApplicationConfiguration;
import org.eclipse.rap.rwt.client.WebClient;

public class BasicApplicationConfiguration implements ApplicationConfiguration{

	@Override
	public void configure(Application application) {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put( WebClient.PAGE_TITLE, "P2 Repository Manager" );
		application.addEntryPoint("/repoman", BasicEntryPoint.class, properties);
	}

}
